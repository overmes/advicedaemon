import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Daemon {
    private static float[] user_basis;
    private static float[] item_basis;
    private static float[][] item_q;
    private static float[][] user_p;
    private static Map<Integer, Integer> itemIndexReplace;
    private static Map<Integer, Integer> userIndexReplace;
    private static float mean;
    private static int factors;
    private static int itemCount;
    private static Map<Integer, Integer> indexItemReplace;
    private static int userCount;
    private static Map<Integer, Integer> indexUserReplace;
    private String RESULT_COL = null;
    private String RESULT_DB = null;
    private String QUEUE_COL = null;
    private String QUEUE_DB = null;
    private final int FACTORS_LEN = 50;
    private final int ADVICE_LEN = 200;
    private MongoClient m;

    public Daemon(String q_db, String q_col, String r_db, String r_col) {
        QUEUE_DB = q_db;
        QUEUE_COL = q_col;
        RESULT_DB = r_db;
        RESULT_COL = r_col;
        m = new MongoClient();

        loadData();
    }

    public Daemon() {
        loadData();
    }

    public void start() throws InterruptedException {
        //main loop
        while (true) {
            System.out.println("loop");

            Map<String, Object> userData = getUserData();
            if (userData != null) {
                try {
                    Map<String, Object> userAdvice = makeAdvice(userData);
                    saveAdvice(userAdvice, userData);
                } catch (Exception ex) {
                    saveError(ex, userData);
                }
                removeQueue(userData);
            } else {
                System.out.println("waiting data");
                Thread.sleep(1000);
            }
        }
    }

    public Map<String, Object> makeAdvice(Map<String, Object> userData) throws InterruptedException {
        Map<String, Object> userModel = getUserModel(userData);
        Map<String, Object> userAdvice = getUserAdvice(userModel, userData);
        List<List<Object>> userNeighbours = getUserNeighbours(userModel);
        userAdvice.put("neighbours", userNeighbours);
        return userAdvice;
    }

    private void loadData() {
        user_basis = (float[]) loadObject("userBasisDone.dat");
        item_basis = (float[]) loadObject("itemBasisDone.dat");
        item_q = (float[][]) loadObject("itemQDone.dat");
        user_p = (float[][]) loadObject("userPDone.dat");

        itemIndexReplace = (Map) loadObject("itemIndexReplace.dat");
        userIndexReplace = (Map) loadObject("userIndexReplace.dat");

        indexItemReplace = (Map) loadObject("indexItemReplace.dat");
        indexUserReplace = (Map) loadObject("indexUserReplace.dat");

        mean = (float) loadObject("mean.dat");

        itemCount = itemIndexReplace.size();
        userCount = userIndexReplace.size();
    }

    private Map<String, Object> getUserData() {

        DB db = m.getDB(QUEUE_DB);
        DBCollection col = db.getCollection(QUEUE_COL);
        DBCursor cursor = col.find().sort(new BasicDBObject("date", 1));
        if (cursor.hasNext()) {
            DBObject doc = cursor.next();
            return doc.toMap();
        }
        return null;

    }

    private Map<String, Object> getUserModel(Map<String, Object> userData) {

        Map<String, List> userScores = (Map<String, List>) userData.get("scores");
        Integer[][] trainDataBox = getTrainData(userScores);
        Integer[] rows = trainDataBox[0];
        Integer[] data = trainDataBox[1];

        float user_temp_basis = 0;
        float[] user_temp_p = new float[FACTORS_LEN];
        Arrays.fill(user_temp_p, 0f);

        float curentError = 50;
        float lastError = 60;
        for (int epoch = 0; curentError < (lastError - 0.0001); epoch++) {

            float sumError = 0;
            for (int i = 0; i < data.length; i++) {

                int score = data[i];
                int itemId = rows[i];

                float predict_value = mean + item_basis[itemId] + user_temp_basis + dot(item_q[itemId], user_temp_p);
                float error = score - predict_value;

                user_temp_basis = user_temp_basis + 0.005f * (error - 0.1f * user_temp_basis);

                float[] qi = item_q[itemId];
                for (int f = 0; f < FACTORS_LEN; f++) {
                    float qif = qi[f];
                    user_temp_p[f] = user_temp_p[f] + 0.005f * (error * qif - 0.1f * user_temp_p[f]);
                }

                sumError += error * error;

            }

            lastError = curentError;
            curentError = (float) Math.sqrt(sumError / data.length);

        }
        Map<String, Object> res = new HashMap<>();
        res.put("user_basis", user_temp_basis);
        res.put("user_p", user_temp_p);
        res.put("error", curentError);

        return res;
    }

    private float dot(float[] x, float[] y) {
        float result = 0;
        for (int i = 0; i < y.length; i++) {
            float d1 = y[i];
            float d2 = x[i];
            result += d1 * d2;
        }
        return result;
    }

    private Map<String, Object> getUserAdvice(Map<String, Object> userModel, Map<String, Object> userData) {
        float user_temp_basis = (float) userModel.get("user_basis");
        float[] user_temp_p = (float[]) userModel.get("user_p");

        Map scores = (Map) userData.get("scores");

        List<List<Object>> adviceListAnime = new ArrayList<>();
        List<List<Object>> adviceListManga = new ArrayList<>();
        List<List<Object>> userTop = new ArrayList<>();

        for (int i = 0; i < itemCount; i++) {
            float predict_value = mean + item_basis[i] + user_temp_basis + dot(item_q[i], user_temp_p);

            Integer itemId = indexItemReplace.get(i);
            if (itemId != null) {
                String strItemId = itemId.toString();
                if (!scores.containsKey(strItemId)) {
                    if (itemId > 0) {
                        addToMaxList(adviceListAnime, predict_value, itemId, ADVICE_LEN);
                    } else {
                        addToMaxList(adviceListManga, predict_value, itemId, ADVICE_LEN);
                    }
                } else {
                    addToMaxList(userTop, predict_value, itemId, 50);
                }
            }


        }

        Map<String, Object> res = new HashMap<>();
        res.put("anime", adviceListAnime);
        res.put("manga", adviceListManga);
        res.put("top", userTop);
        res.put("pred_error", userModel.get("error"));
        return res;
    }

    private float diveuclid(float[] f0, float[] f1) {
        float sum = 0;
        for (int i = 0; i < FACTORS_LEN; i++) {
            sum += (f0[i] - f1[i]) * (f0[i] - f1[i]);
        }

        return 1 / (float) Math.sqrt(sum);
    }

    private void saveAdvice(Map<String, Object> userAdvice, Map<String, Object> userData) {
        DB db = m.getDB(RESULT_DB);
        DBCollection col = db.getCollection(RESULT_COL);
        String userId = (String) userData.get("_id");

        BasicDBObject doc = new BasicDBObject(userAdvice)
                .append("name", userId).append("scores", userData.get("scores"))
                .append("date", userData.get("date"));
        col.save(doc);
    }

    private List<List<Object>> getUserNeighbours(Map<String, Object> userModel) {
        List<List<Object>> neighbours = new ArrayList<>();
        float[] user_temp_p = (float[]) userModel.get("user_p");
        float user_temp_basis = (float) userModel.get("user_basis");
        for (int i = 0; i < user_p.length; i++) {
            float[] neighbour_p = user_p[i];
            float neighbour_basis = user_basis[i];
            float dist = diveuclid(user_temp_p, neighbour_p);
            Integer userId = indexUserReplace.get(i);
            addToMaxList(neighbours, dist, userId, 200);
        }

        return neighbours;
    }

    public void saveObject(String fileName, Object list) {
        try {
            FileOutputStream fout = new FileOutputStream("data/" + fileName);
            try (ObjectOutputStream oos = new ObjectOutputStream(fout)) {
                oos.writeObject(list);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Object loadObject(String fileName) {
        try {
            FileInputStream fin = new FileInputStream("data/" + fileName);
            Object data;
            try (ObjectInputStream ois = new ObjectInputStream(fin)) {
                data = ois.readObject();
            }
            return data;
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Integer[][] getTrainData(Map<String, List> userScores) {

        List<Integer> rows_list = new ArrayList<>();
        List<Integer> data_list = new ArrayList<>();
        for (Map.Entry<String, List> entry : userScores.entrySet()) {
            int itemId = Integer.parseInt(entry.getKey());
            List scoreData = entry.getValue();
            int score = (int) scoreData.get(0);
            int type = (int) scoreData.get(1);

            if (itemIndexReplace.containsKey(itemId) && score > 0) {
                int itemIndex = itemIndexReplace.get(itemId);
                rows_list.add(itemIndex);
                data_list.add(score);
            }
        }
        Integer[] rows = rows_list.toArray(new Integer[rows_list.size()]);
        Integer[] data = data_list.toArray(new Integer[data_list.size()]);
        Integer[][] box = new Integer[2][];
        box[0] = rows;
        box[1] = data;
        return box;
    }

    private void addToMaxList(List<List<Object>> adviceListAnime, float predict_value, int itemId, int len) {
        boolean addedFlag = false;
        for (int i = 0; i < adviceListAnime.size(); i++) {
            Integer tempItemId = (Integer) adviceListAnime.get(i).get(0);
            Float itemScore = (Float) adviceListAnime.get(i).get(1);
            if (predict_value > itemScore) {
                List<Object> tempList = new ArrayList<>();
                tempList.add(itemId);
                tempList.add(predict_value);
                adviceListAnime.add(i, tempList);
                if (adviceListAnime.size() > len) {
                    adviceListAnime.remove(adviceListAnime.size() - 1);
                }
                addedFlag = true;
                break;
            }
        }

        if (!addedFlag && adviceListAnime.size() < len) {
            List<Object> tempList = new ArrayList<>();
            tempList.add(itemId);
            tempList.add(predict_value);
            adviceListAnime.add(tempList);
        }
    }

    private void saveError(Exception ex, Map<String, Object> userData) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        ex.printStackTrace(pw);

        DB db = m.getDB(RESULT_DB);
        DBCollection col = db.getCollection("error");
        BasicDBObject doc = new BasicDBObject(userData).append("stack", sw.toString()).append("message", ex.getMessage());
        col.save(doc);
    }

    public void saveItemCompatibility() {
        DB db = m.getDB(RESULT_DB);
        DBCollection col = db.getCollection("compatibility");

        for (int itemIndex = 0; itemIndex < item_q.length; itemIndex++) {

            float[] itemSingular = item_q[itemIndex];

            List<List<Object>> compatibilityAnime = new ArrayList<>();
            List<List<Object>> compatibilityManga = new ArrayList<>();


            for (int neighbourIndex = 0; neighbourIndex < item_q.length; neighbourIndex++) {
                float[] neighbourSingular = item_q[neighbourIndex];
                if (itemIndex != neighbourIndex) {
                    float divdist = diveuclid(itemSingular, neighbourSingular);
                    int neighbourId = indexItemReplace.get(neighbourIndex);
                    if (neighbourId > 0) {
                        addToMaxList(compatibilityAnime, divdist, neighbourId, 50);
                    } else {
                        addToMaxList(compatibilityManga, divdist, neighbourId, 50);
                    }

                }
            }

            int itemId = indexItemReplace.get(itemIndex);
            BasicDBObject doc = new BasicDBObject("_id", itemId).append("anime", compatibilityAnime).append("manga", compatibilityManga);
            col.save(doc);

        }

    }

    public List<List<Object>> getCompatibleItems (int ItemId) {
        Integer itemIndex = itemIndexReplace.get(ItemId);
        List<List<Object>> compatibilityList = new ArrayList<>();
        if (itemIndex != null) {
            float[] itemSingular = item_q[itemIndex];
            for (int neighbourIndex = 0; neighbourIndex < item_q.length; neighbourIndex++) {
                float[] neighbourSingular = item_q[neighbourIndex];
                if (itemIndex != neighbourIndex) {
                    float divdist = diveuclid(itemSingular, neighbourSingular);
                    int neighbourId = indexItemReplace.get(neighbourIndex);
                    addToMaxList(compatibilityList, divdist, neighbourId, 50);
                }
            }
        }

        return compatibilityList;
    }

    private void removeQueue(Map<String, Object> userData) {
        DB db = m.getDB(QUEUE_DB);
        DBCollection col = db.getCollection(QUEUE_COL);
        col.remove(new BasicDBObject("_id", userData.get("_id")));
    }
}
