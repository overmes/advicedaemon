public class AdviceDaemon2 {

    public static void main(String[] args) throws InterruptedException {

        String QUEUE_DB = "site";
        String QUEUE_COL = "queue";
        String RESULT_DB = "site";
        String RESULT_COL = "result";

        AdviceDaemon2.runDaemon(QUEUE_DB, QUEUE_COL, RESULT_DB, RESULT_COL);
    }

    public static void countCompatibility(String QUEUE_DB,String QUEUE_COL,String RESULT_DB,String RESULT_COL) {
        Daemon daemon = new Daemon(QUEUE_DB, QUEUE_COL, RESULT_DB, RESULT_COL);
        daemon.saveItemCompatibility();
    }

    public static void runDaemon(String QUEUE_DB,String QUEUE_COL,String RESULT_DB,String RESULT_COL) throws InterruptedException {
        Daemon daemon = new Daemon(QUEUE_DB, QUEUE_COL, RESULT_DB, RESULT_COL);
        daemon.start();
    }
}
