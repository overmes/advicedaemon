import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import org.apache.commons.cli.*;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.thread.QueuedThreadPool;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

public class AdviceServer {

    public static void main(String[] args) throws Exception {
        Options options = new Options();

        Option port = OptionBuilder.withArgName("port")
                .hasArg()
                .isRequired()
                .withType(Number.class)
                .create("p");
        Option threads = OptionBuilder.withArgName("threads")
                .hasArg()
                .isRequired()
                .withType(Number.class)
                .create("t");
        options.addOption(port);
        options.addOption(threads);

        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp( "server [options]", options );
        CommandLineParser parser = new GnuParser();
        try {
            CommandLine line = parser.parse( options, args );

            runServer(((Number)line.getParsedOptionValue("p")).intValue(), ((Number)line.getParsedOptionValue("t")).intValue());
        }
        catch( ParseException exp ) {
            System.err.println( "Parsing failed.  Reason: " + exp.getMessage() );
        }

    }

    public static void runServer(Integer port, Integer threads) throws Exception {
        QueuedThreadPool threadPool = new QueuedThreadPool();
        threadPool.setMaxThreads(threads);
        Server server = new Server(threadPool);

        ServerConnector http = new ServerConnector(server);
        http.setHost("127.0.0.1");
        http.setPort(port);
        http.setIdleTimeout(30000);
        server.addConnector(http);

        Daemon daemon = new Daemon();
        ServletHandler handler = new ServletHandler();
        AdviceServlet adviceServlet = new AdviceServlet(daemon);
        ServletHolder adviceServletHolder = new ServletHolder(adviceServlet);
        handler.addServletWithMapping(adviceServletHolder, "/getAdvice");

        CompatibleServlet compatibleServlet = new CompatibleServlet(daemon);
        ServletHolder compatibleServletHolder = new ServletHolder(compatibleServlet);
        handler.addServletWithMapping(compatibleServletHolder, "/compatibility");

        server.setHandler(handler);
        server.start();
        server.join();
    }

    @SuppressWarnings("serial")
    public static class AdviceServlet extends HttpServlet {

        private Daemon daemon;

        public AdviceServlet(Daemon daemon) {
            this.daemon = daemon;
        }

        @Override
        protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            InputStream body = request.getInputStream();
            String stringBody = convertStreamToString(body);
            Object o = com.mongodb.util.JSON.parse(stringBody);
            DBObject dbObj = (DBObject) o;
            response.setContentType("application/javascript");

            try {
                Map<String, Object> advice = daemon.makeAdvice((Map<String, Object>) dbObj.toMap());
                BasicDBObject adviceDBObject = new BasicDBObject(advice);
                response.setStatus(HttpServletResponse.SC_OK);
                response.getWriter().println(adviceDBObject.toString());
            } catch (InterruptedException e) {
                e.printStackTrace();
                response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            }
        }
    }

    @SuppressWarnings("serial")
    public static class CompatibleServlet extends HttpServlet {

        private Daemon daemon;

        public CompatibleServlet(Daemon daemon) {
            this.daemon = daemon;
        }

        @Override
        protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
            Integer itemId = Integer.valueOf(request.getParameter("itemid"));
            List<List<Object>> items = daemon.getCompatibleItems(itemId);
            BasicDBObject itemsDBObject = new BasicDBObject("_id", itemId).append("items", items);
            response.setStatus(HttpServletResponse.SC_OK);
            response.getWriter().println(itemsDBObject.toString());
        }
    }

    static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }
}
